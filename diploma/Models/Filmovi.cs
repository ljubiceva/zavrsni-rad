﻿using System;
using diploma.Models;

namespace diploma.Models {
    public class Filmovi
    {
        public int Idfilmovi { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Lokacija { get; set; }
        public string Zanr { get; set; }
        public string Imdb { get; set; }
        public string Trejler { get; set; }
        public string kritika { get; set; }
        public string OpisId { get; set; }
        public string Datum { get; set; }
        public string myTime { get; set; }


    }
    public class FilmoviId
    {
        public int Idfilmovi { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Lokacija { get; set; }
        public string Zanr { get; set; }
        public string Imdb { get; set; }
        public string Trejler { get; set; }
        public string kritika { get; set; }
        public string OpisId { get; set; }
        public string Datum { get; set; }
        public string myTime { get; set; }

    }
    public class NewFilmoviId
    {
        public int Idfilmovi { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Lokacija { get; set; }
        public string Zanr { get; set; }
        public string Imdb { get; set; }
        public string Trejler { get; set; }
        public string kritika { get; set; }
        public string OpisId { get; set; }
        public string Datum { get; set; }
        public string myTime { get; set; }

    }
}

