 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class FilmoviContext
    {
        public string ConnectionString { get; set; }
        public FilmoviContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Filmovi> GetAllFilmovi()
        {
            List<Filmovi> list = new List<Filmovi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM filmovi ORDER BY datum DESC"  , conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        list.Add(new Filmovi()
                        {
                            Idfilmovi = reader.GetInt32("Idfilmovi"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Zanr = reader.GetString("Zanr"),
                            Imdb = reader.GetString("Imdb"),
                            Trejler = reader.GetString("Trejler"),
                            kritika = reader.GetString("kritika"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")
                        });
                    }
                }
            }
            return list;
        }
        public List<Filmovi> GetFilmoviId(int id)
        {
            List<Filmovi> list = new List<Filmovi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM filmovi where idfilmovi=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Filmovi()
                        {
                            Idfilmovi = reader.GetInt32("Idfilmovi"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Zanr = reader.GetString("Zanr"),
                            Imdb = reader.GetString("Imdb"),
                            Trejler = reader.GetString("Trejler"),
                            kritika = reader.GetString("kritika"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }
        public int Izbrisifilm(int id)
        {
           
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM filmovi where Idfilmovi=@Idfilmovi", conn);


                cmd.Parameters.AddWithValue("Idfilmovi", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }

        public int Unosfilmovi(Filmovi filmovi)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into filmovi (Naziv, Opis, Slika, Lokacija, Zanr, Imdb, Trejler, kritika, OpisId, Datum, myTime) VALUES (@Naziv, @Opis, @Slika, @Lokacija, @Zanr, @Imdb, @Trejler, @kritika, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", filmovi.Naziv);
                cmd.Parameters.AddWithValue("@Opis", filmovi.Opis);
                cmd.Parameters.AddWithValue("@Slika", filmovi.Slika);
                cmd.Parameters.AddWithValue("@Lokacija", filmovi.Lokacija);
                cmd.Parameters.AddWithValue("@Zanr", filmovi.Zanr);
                cmd.Parameters.AddWithValue("@Imdb", filmovi.Imdb);
                cmd.Parameters.AddWithValue("@Trejler", filmovi.Trejler);
                cmd.Parameters.AddWithValue("@kritika", filmovi.kritika);
                cmd.Parameters.AddWithValue("@OpisId", filmovi.OpisId);
                cmd.Parameters.AddWithValue("@Datum", filmovi.Datum); 
                cmd.Parameters.AddWithValue("@myTime", filmovi.myTime);



                cmd.ExecuteNonQuery();
                conn.Close();

            return 1;
            }
        }
        
    }
}
