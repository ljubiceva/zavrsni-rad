using System;
using diploma.Models;

namespace diploma.Models
{
    public class Izlozbe
    {
      
        public int Idizlozbe { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Slika1 { get; set; }
        public string Slika2 { get; set; }
        public string Slika3 { get; set; }
        public string Lokacija { get; set; }
        public string OpisId { get; set; }
        public string Datum { get; set; }
        public string myTime { get; set; }

    }

}

