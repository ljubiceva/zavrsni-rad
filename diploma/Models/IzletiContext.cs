using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class IzletiContext
    {
        public string ConnectionString { get; set; }
        public IzletiContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Izleti> GetAllIzleti()
        {
            List<Izleti> list = new List<Izleti>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM Izleti  ORDER BY datum DESC", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Izleti()
                        {
                            Idizleti = reader.GetInt32("Idizleti"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }
        public List<Izleti> GetIzletiId(int id)
        {
            List<Izleti> list = new List<Izleti>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM izleti where idizleti=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Izleti()
                        {
                            Idizleti = reader.GetInt32("Idizleti"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }

        public int Izbrisiizlet(int id)
        {

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM izleti where Idizleti=@Idizleti", conn);


                cmd.Parameters.AddWithValue("Idizleti", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }


        public int Unosizleti(Izleti izleti)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into izleti (Naziv, Opis, Slika,Lokacija, Trejler, OpisId, Datum, myTime) VALUES (@Naziv, @Opis, @Slika, @Lokacija, @Trejler, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", izleti.Naziv);
                cmd.Parameters.AddWithValue("@Opis", izleti.Opis);
                cmd.Parameters.AddWithValue("@Slika", izleti.Slika);
                cmd.Parameters.AddWithValue("@Lokacija", izleti.Lokacija);
                cmd.Parameters.AddWithValue("@Trejler", izleti.Trejler);
                cmd.Parameters.AddWithValue("@OpisId", izleti.OpisId);
                cmd.Parameters.AddWithValue("@Datum", izleti.Datum);
                cmd.Parameters.AddWithValue("@myTime", izleti.myTime);
                
                cmd.ExecuteNonQuery();
                conn.Close();


                return 1;
            }
        }
    }
}