using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class PozoristeContext
    {
        public string ConnectionString { get; set; }
        public PozoristeContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Pozoriste> GetAllPozoriste()
        {
            List<Pozoriste> list = new List<Pozoriste>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM pozoriste ORDER BY datum DESC", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Pozoriste()
                        {
                            Idpozoriste = reader.GetInt32("Idpozoriste"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }
        public List<Pozoriste> GetPozoristeId(int id)
        {
            List<Pozoriste> list = new List<Pozoriste>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM pozoriste where idpozoriste=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Pozoriste()
                        {
                            Idpozoriste = reader.GetInt32("Idpozoriste"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }

        public int Izbrisir(int id)
        {

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM pozoriste where Idpozoriste=@Idpozoriste", conn);


                cmd.Parameters.AddWithValue("Idpozoriste", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }

        public int Unospozoriste(Pozoriste pozoriste)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into pozoriste (Naziv, Opis, Slika, Lokacija, Trejler, OpisId, Datum, myTime ) VALUES (@Naziv, @Opis, @Slika, @Lokacija, @Trejler, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", pozoriste.Naziv);
                cmd.Parameters.AddWithValue("@Opis", pozoriste.Opis);
                cmd.Parameters.AddWithValue("@Slika", pozoriste.Slika);
                cmd.Parameters.AddWithValue("@Lokacija", pozoriste.Lokacija);
                cmd.Parameters.AddWithValue("@Trejler", pozoriste.Trejler);
                cmd.Parameters.AddWithValue("@OpisId", pozoriste.OpisId);
                cmd.Parameters.AddWithValue("@Datum", pozoriste.Datum);
                cmd.Parameters.AddWithValue("@myTime", pozoriste.myTime);




                cmd.ExecuteNonQuery();
                conn.Close();


                return 1;
            }
        }
    }
}