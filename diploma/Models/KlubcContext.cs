using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class KlubcContext
    {
        public string ConnectionString { get; set; }
        public KlubcContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Klubc> GetAllKlubc()
        {
            List<Klubc> list = new List<Klubc>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM klubc ORDER BY datum DESC", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Klubc()
                        {
                            Idklubc = reader.GetInt32("Idklubc"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }
        public List<Klubc> GetKlubcid(int id)
        {
            List<Klubc> list = new List<Klubc>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM klubc where idklubc=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Klubc()
                        {
                            Idklubc = reader.GetInt32("Idklubc"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Lokacija = reader.GetString("Lokacija"),
                            Trejler = reader.GetString("Trejler"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }

        public int Izbrisiklubc(int id)
        {

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM klubc where Idklubc=@Idklubc", conn);


                cmd.Parameters.AddWithValue("Idklubc", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }

        public int Unosklubc(Klubc klubc)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into klubc (Naziv, Opis, Slika, Lokacija, Trejler, OpisId, Datum, myTime) VALUES (@Naziv, @Opis, @Slika, @Lokacija, @Trejler, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", klubc.Naziv);
                cmd.Parameters.AddWithValue("@Opis", klubc.Opis);
                cmd.Parameters.AddWithValue("@Slika", klubc.Slika);
                cmd.Parameters.AddWithValue("@Lokacija", klubc.Lokacija);
                cmd.Parameters.AddWithValue("@Trejler", klubc.Trejler);
                cmd.Parameters.AddWithValue("@OpisId", klubc.OpisId);
                cmd.Parameters.AddWithValue("@Datum", klubc.Datum);
                cmd.Parameters.AddWithValue("@myTime", klubc.myTime);




                cmd.ExecuteNonQuery();
                conn.Close();


                return 1;
            }
        }
    }
}