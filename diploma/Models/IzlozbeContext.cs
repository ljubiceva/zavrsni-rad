using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class IzlozbeContext
    {
        public string ConnectionString { get; set; }
        public IzlozbeContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Izlozbe> GetAllIzlozbe()
        {
            List<Izlozbe> list = new List<Izlozbe>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM izlozbe ORDER BY datum DESC", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Izlozbe()
                        {
                            Idizlozbe = reader.GetInt32("Idizlozbe"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Slika1 = reader.GetString("Slika1"),
                            Slika2 = reader.GetString("Slika2"),
                            Slika3 = reader.GetString("Slika3"),
                            Lokacija = reader.GetString("Lokacija"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")


                        });
                    }
                }
            }
            return list;
        }

        public List<Izlozbe> GetIzlozbeId(int id)
        {
            List<Izlozbe> list = new List<Izlozbe>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM izlozbe where idizlozbe=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Izlozbe()
                        {
                            Idizlozbe = reader.GetInt32("Idizlozbe"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Slika1 = reader.GetString("Slika1"),
                            Slika2 = reader.GetString("Slika2"),
                            Slika3 = reader.GetString("Slika3"),
                            Lokacija = reader.GetString("Lokacija"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")
                        });
                    }
                }
            }
            return list;
        }

        public int Izbrisiizlozbu(int id)
        {

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM izlozbe where Idizlozbe=@Idizlozbe", conn);


                cmd.Parameters.AddWithValue("Idizlozbe", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }

        public int Unosizlozba(Izlozbe izlozbe)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into izlozbe (Naziv, Opis, Slika, Slika1, Slika2, Slika3, Lokacija, OpisId, Datum, myTime) VALUES (@Naziv, @Opis, @Slika, @Slika1, @Slika2, @Slika3, @Lokacija, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", izlozbe.Naziv);
                cmd.Parameters.AddWithValue("@Opis", izlozbe.Opis);
                cmd.Parameters.AddWithValue("@Slika", izlozbe.Slika);
                cmd.Parameters.AddWithValue("@Slika1", izlozbe.Slika1);
                cmd.Parameters.AddWithValue("@Slika2", izlozbe.Slika2);
                cmd.Parameters.AddWithValue("@Slika3", izlozbe.Slika3);
                cmd.Parameters.AddWithValue("@Lokacija", izlozbe.Lokacija);
                cmd.Parameters.AddWithValue("@OpisId", izlozbe.OpisId);
                cmd.Parameters.AddWithValue("@Datum", izlozbe.Datum);
                cmd.Parameters.AddWithValue("@myTime", izlozbe.myTime);



                cmd.ExecuteNonQuery();
                conn.Close();


                return 1;
            }
        }
    }
}