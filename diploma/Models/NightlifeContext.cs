using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;


namespace diploma.Models
{
    public class NightlifeContext
    {
        public string ConnectionString { get; set; }
        public NightlifeContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<Nightlife> GetAllNightlife()
        {
            List<Nightlife> list = new List<Nightlife>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM nl ORDER BY datum DESC", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Nightlife()
                        {
                            Idnl = reader.GetInt32("Idnl"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Trejler = reader.GetString("Trejler"),
                            Lokacija = reader.GetString("Lokacija"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }

        public List<Nightlife> GetNightlId(int id)
        {
            List<Nightlife> list = new List<Nightlife>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM nl where idnl=" + id, conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Nightlife()
                        {
                            Idnl = reader.GetInt32("Idnl"),
                            Naziv = reader.GetString("Naziv"),
                            Opis = reader.GetString("Opis"),
                            Slika = reader.GetString("Slika"),
                            Trejler = reader.GetString("Trejler"),
                            Lokacija = reader.GetString("Lokacija"),
                            OpisId = reader.GetString("OpisId"),
                            Datum = reader.GetString("Datum"),
                            myTime = reader.GetString("myTime")

                        });
                    }
                }
            }
            return list;
        }

        public int Izbrisinl(int id)
        {

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM nl where Idnl=@Idnl", conn);


                cmd.Parameters.AddWithValue("Idnl", id);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            return 1;
        }

        public int Unosnl(Nightlife nightlife)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(" INSERT into nl (Naziv, Opis, Slika,Trejler, Lokacija, OpisId, Datum, myTime) VALUES (@Naziv, @Opis, @Slika, @Trejler, @Lokacija, @OpisId, @Datum, @myTime)", conn);


                cmd.Parameters.AddWithValue("@Naziv", nightlife.Naziv);
                cmd.Parameters.AddWithValue("@Opis", nightlife.Opis);
                cmd.Parameters.AddWithValue("@Slika", nightlife.Slika);
                cmd.Parameters.AddWithValue("@Trejler", nightlife.Trejler);
                cmd.Parameters.AddWithValue("@Lokacija", nightlife.Lokacija);
                cmd.Parameters.AddWithValue("@OpisId", nightlife.OpisId);
                cmd.Parameters.AddWithValue("@Datum", nightlife.Datum);
                cmd.Parameters.AddWithValue("@myTime", nightlife.myTime);




                cmd.ExecuteNonQuery();
                conn.Close();


                return 1;
            }
        }
    }
}