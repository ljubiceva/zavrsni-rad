using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace diploma.Controllers
{
    public class KlubcController : Controller
    {
        //get all
        public ActionResult<IEnumerable<Klubc>> Citalac()
        {

            KlubcContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.KlubcContext)) as KlubcContext;

            return context.GetAllKlubc();
        }

        //get id
        public ActionResult<IEnumerable<Klubc>> Citalacid(int id)
        {

            KlubcContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.KlubcContext)) as KlubcContext;

            return context.GetKlubcid(id);
        }

        //post
        public int Postklubc([FromBody] Klubc klubc)
        {

            KlubcContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.KlubcContext)) as KlubcContext;

            return context.Unosklubc(klubc);
        }
        //delete
        public int Izbrisikc(int id)
        {
            KlubcContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.KlubcContext)) as KlubcContext;

            return context.Izbrisiklubc(id);
        }
    }
}