using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace diploma.Controllers
{
    public class IzlozbeController : Controller
    {
        [HttpGet]
        public ActionResult<IEnumerable<Izlozbe>> Izlozbe()
        {

            IzlozbeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzlozbeContext)) as IzlozbeContext;

            return context.GetAllIzlozbe();
        }

        public ActionResult<IEnumerable<Izlozbe>> Izlozba(int id)
        {
            IzlozbeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzlozbeContext)) as IzlozbeContext;

            return context.GetIzlozbeId(id);
        }

        public int Postizlozba([FromBody] Izlozbe izlozbe)
        {

            IzlozbeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzlozbeContext)) as IzlozbeContext;

            return context.Unosizlozba(izlozbe);
        }

        public int Izbrisiizb(int id)
        {
            IzlozbeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzlozbeContext)) as IzlozbeContext;

            return context.Izbrisiizlozbu(id);
        }

    }
}