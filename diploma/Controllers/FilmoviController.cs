using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace diploma.Controllers
{   

    public class FilmoviController : Controller
    {
         //[Route("api/naslovi")]
        public ActionResult<IEnumerable<Filmovi>> Naslovi()
        {

            FilmoviContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.FilmoviContext)) as FilmoviContext;


            return context.GetAllFilmovi();
        }


        public ActionResult<IEnumerable<Filmovi>> Naslov(int id)
        {
            FilmoviContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.FilmoviContext)) as FilmoviContext;

            return context.GetFilmoviId(id);
        }


        //[Route("api/postfilmovi")]
            
            public int Postfilmovi([FromBody] Filmovi filmovi ){
             
            FilmoviContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.FilmoviContext)) as FilmoviContext;
            
            return context.Unosfilmovi(filmovi);
        }

        //[Route("api/naslovi/izbrisi/{id}")]
        public int Izbrisi(int id)
        {
            FilmoviContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.FilmoviContext)) as FilmoviContext;

            return context.Izbrisifilm(id);
        }

        
     }
}

            
    
