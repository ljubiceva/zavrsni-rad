using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace diploma.Controllers
{
    public class PozoristeController : Controller
    {
        [HttpGet]
        public ActionResult<IEnumerable<Pozoriste>> Repertoar()
        {

            PozoristeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.PozoristeContext)) as PozoristeContext;

            return context.GetAllPozoriste();
        }

        public ActionResult<IEnumerable<Pozoriste>> Repertoarid(int id)
        {
            PozoristeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.PozoristeContext)) as PozoristeContext;

            return context.GetPozoristeId(id);
        }

        public int Postpozoriste([FromBody] Pozoriste pozoriste)
        {

           PozoristeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.PozoristeContext)) as PozoristeContext;

            return context.Unospozoriste(pozoriste);
        }
        //izbrisi
        public int Izbrisir(int id)
        {
            PozoristeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.PozoristeContext)) as PozoristeContext;

            return context.Izbrisir(id);
        }
        
    }
}