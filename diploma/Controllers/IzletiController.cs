using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace diploma.Controllers
{   
    public class IzletiController : Controller
    {
        public ActionResult<IEnumerable<Izleti>> Izleti()
        {

            IzletiContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzletiContext)) as IzletiContext;

            return context.GetAllIzleti();
        }

        public ActionResult<IEnumerable<Izleti>> Izletiid(int id)
        {
            IzletiContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzletiContext)) as IzletiContext;

            return context.GetIzletiId(id);
        }

        public int Postizleti([FromBody] Izleti izleti)
            {
            IzletiContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models. IzletiContext)) as  IzletiContext;
            
            return context.Unosizleti(izleti);
            }
        
        //izbrisi
        public int Izbrisii(int id)
        {
            IzletiContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.IzletiContext)) as IzletiContext;

            return context.Izbrisiizlet(id);
        }


    }
}