using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using diploma.Models;
using diploma.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace diploma.Controllers
{
    public class NightlifeController : Controller
    {

        public ActionResult<IEnumerable<Nightlife>> Nightlife()
        {

            NightlifeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.NightlifeContext)) as NightlifeContext;

            return context.GetAllNightlife();
        }

        public ActionResult<IEnumerable<Nightlife>> Nlid(int id)
        {
            NightlifeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.NightlifeContext)) as NightlifeContext;

            return context.GetNightlId(id);
        }

        public int Postnl([FromBody] Nightlife nightlife)
        {

            NightlifeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.NightlifeContext)) as NightlifeContext;

            return context.Unosnl(nightlife);
        }

        //delete
        public int Izbrisinl(int id)
        {
           NightlifeContext context = HttpContext.RequestServices.GetService(typeof(diploma.Models.NightlifeContext)) as NightlifeContext;

            return context.Izbrisinl(id);
        }
    }
}