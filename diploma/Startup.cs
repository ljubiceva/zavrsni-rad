﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.AspNetCore.Routing;
using diploma.Models;


namespace diploma
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSpaStaticFiles(c => { c.RootPath = "dist/ar-dogadjaji"; });
            services.AddRouting(opt => opt.LowercaseUrls = true);
            services.Add(new ServiceDescriptor(typeof(FilmoviContext), new FilmoviContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(IzletiContext), new IzletiContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(PozoristeContext), new PozoristeContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(IzlozbeContext), new IzlozbeContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(KlubcContext), new KlubcContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(NightlifeContext), new NightlifeContext(Configuration.GetConnectionString("DefaultConnection"))));
        }

        
 
            


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseMvc(routes => {

                    routes.MapRoute(
                    name: "Default", 
                    template: "api/{controller}/{action=index}/{id}"
                    );
                    
                    //filmovi
                    routes.MapRoute(
                    name: "Naslovi",
                    template: "api/naslovi/",
                    defaults: new { controller = "Filmovi", action = "Naslovi"}
                    );
                    routes.MapRoute(
                    name: "Naslov",
                    template: "api/naslovi/{id}",
                    defaults: new { controller = "Filmovi", action = "Naslov"}
                    );
                    routes.MapRoute(
                    name: "Postfilmovi",
                    template: "api/postfilmovi/",
                    defaults: new { controller = "Filmovi", action = "Postfilmovi"}
                    );
                    routes.MapRoute(
                    name: "Izbrisi",
                    template: "api/naslovi/izbrisi/{id}",
                    defaults: new { controller = "Filmovi", action = "Izbrisi"}
                    );

                    //izleti
                    routes.MapRoute(
                    name: "Izleti",
                    template: "api/izleti/",
                    defaults: new { controller = "Izleti", action = "Izleti"}
                    );
                    routes.MapRoute(
                    name: "Izletiid",
                    template: "api/izleti/{id}",
                    defaults: new { controller = "Izleti", action = "Izletiid"}
                    );
                    routes.MapRoute(
                    name: "Postizleti",
                    template: "api/postizleti/",
                    defaults: new { controller = "Izleti", action = "Postizleti"}
                    );
                    routes.MapRoute(
                    name: "Izbrisii",
                    template: "api/izleti/izbrisii/{id}",
                    defaults: new { controller = "Izleti", action = "Izbrisii"}
                    );

                    //pozoriste
                    routes.MapRoute(
                    name: "Repertoar",
                    template: "api/repertoar/",
                    defaults: new { controller = "Pozoriste", action = "Repertoar"}
                    );
                    routes.MapRoute(
                    name: "Repertoarid",
                    template: "api/repertoar/{id}",
                    defaults: new { controller = "Pozoriste", action = "Repertoarid"}
                    );
                    routes.MapRoute(
                    name: "Postpozoriste",
                    template: "api/postpozoriste/",
                    defaults: new { controller = "Pozoriste", action = "Postpozoriste"}
                    );
                    routes.MapRoute(
                    name: "Izbrisir",
                    template: "api/repertoar/izbrisip/{id}",
                    defaults: new { controller = "Pozoriste", action = "Izbrisir"}
                    );

                    //izlozbe
                    routes.MapRoute(
                    name: "Izlozbe",
                    template: "api/izlozbe/",
                    defaults: new { controller = "Izlozbe", action = "Izlozbe"}
                    );
                    routes.MapRoute(
                    name: "Izlozba",
                    template: "api/izlozbe/{id}",
                    defaults: new { controller = "Izlozbe", action = "Izlozba"}
                    );
                    routes.MapRoute(
                    name: "Izbrisiizb",
                    template: "api/izlozbe/izbrisiizb/{id}",
                    defaults: new { controller = "Izlozbe", action = "Izbrisiizb"}
                    );
                    routes.MapRoute(
                    name: "Postizlozba",
                    template: "api/postizlozba/",
                    defaults: new { controller = "Izlozbe", action = "Postizlozba"}
                    );
                    
                    
                    //klub citalaca
                    routes.MapRoute(
                    name: "Citalac",
                    template: "api/citalac/",
                    defaults: new { controller = "Klubc", action = "Citalac"}
                    );
                    routes.MapRoute(
                    name: "Citalacid",
                    template: "api/citalac/{id}",
                    defaults: new { controller = "Klubc", action = "Citalacid"}
                    );
                    routes.MapRoute(
                    name: "Postklubc",
                    template: "api/postklubc/",
                    defaults: new { controller = "Klubc", action = "Postklubc"}
                    );
                    routes.MapRoute(
                    name: "Izbrisikc",
                    template: "api/citalac/izbrisikc/{id}",
                    defaults: new { controller = "Klubc", action = "Izbrisikc"}
                    );
                    
                    //night life
                    routes.MapRoute(
                    name: "Nightlife",
                    template: "api/nightlife/",
                    defaults: new { controller = "Nightlife", action = "Nightlife"}
                    );
                    routes.MapRoute(
                    name: "Nlid",
                    template: "api/nightlife/{id}",
                    defaults: new { controller = "Nightlife", action = "Nlid"}
                    );
                    routes.MapRoute(
                    name: "Postnl",
                    template: "api/postnl/",
                    defaults: new { controller = "Nightlife", action = "Postnl"}
                    );
                    routes.MapRoute(
                    name: "Izbrisinl",
                    template: "api/nightlife/izbrisinl/{id}",
                    defaults: new { controller = "Nightlife", action = "Izbrisinl"}
                    );
                   


            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "D:/dot/diploma/ar-dogadjaji";
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
