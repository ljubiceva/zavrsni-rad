import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./pages/home/home.component";
import { UnosDogadjajaComponent } from "./pages/unos-dogadjaja/unos-dogadjaja.component";
import { RegistracijaComponent } from "./pages/registracija/registracija.component";
import { LoginComponent } from "./pages/login/login.component";
import { PretragaDogadjajaComponent } from "./pages/pretraga-dogadjaja/pretraga-dogadjaja.component";
import { BioskopComponent } from "./components/bioskop/bioskop.component";
import { IzletiComponent } from "./components/izleti/izleti.component";
import { IzlozbeComponent } from "./components/izlozbe/izlozbe.component";
import { PozoristeComponent } from "./components/pozoriste/pozoriste.component";
import { NightlifeComponent } from "./components/nightlife/nightlife.component";
import { KlubCitalacaComponent } from "./components/klub-citalaca/klub-citalaca.component";
// import { AuthGuard } from "./auth-guard.service";
import { FooterComponent } from "./components/footer/footer.component";
import { LogoutComponent } from "./pages/login/logout/logout.component";
import { FilmoviComponent } from "./components/bioskop/filmovi/filmovi.component";
import { IzletiidComponent } from "./components/izleti/izletiid/izletiid.component";
import { PozoristeidComponent } from "./components/pozoriste/pozoristeid/pozoristeid.component";
import { IzlozbaComponent } from "./components/izlozbe/izlozba/izlozba.component";
import { NlidComponent } from "./components/nightlife/nlid/nlid.component";
import { KlubcidComponent } from "./components/klub-citalaca/klubcid/klubcid.component";
import { KontaktComponent } from "./pages/kontakt/kontakt.component";
import { PostfilmoviComponent } from "./components/bioskop/postfilmovi/postfilmovi.component";
import { PostizletiComponent } from "./components/izleti/postizleti/postizleti.component";
import { PostizlozbaComponent } from "./components/izlozbe/postizlozba/postizlozba.component";
import { PostklubcComponent } from "./components/klub-citalaca/postklubc/postklubc.component";
import { PostnlComponent } from "./components/nightlife/postnl/postnl.component";
import { PostpozoristeComponent } from "./components/pozoriste/postpozoriste/postpozoriste.component";

const appRoutes: Routes = [
    {path: '', redirectTo: '/home', pathMatch:'full'},
    {path: 'home', component: HomeComponent},
    {path: 'unos-dogadjaja',// canActivate: [AuthGuard],
    component: UnosDogadjajaComponent, children:[ 
        {path: '', redirectTo: 'postfilmovi', pathMatch:'full'},
        {path:'postfilmovi', component: PostfilmoviComponent},
        {path: 'postpozoriste', component: PostpozoristeComponent},
        {path: 'postizleti', component: PostizletiComponent},
        {path: 'postizlozba', component: PostizlozbaComponent},
        {path: 'postnl', component: PostnlComponent},
        {path: 'postklubc', component: PostklubcComponent},

    ]},
    {path: 'registracija', component: RegistracijaComponent},
    {path: 'pretraga-dogadjaja', component: PretragaDogadjajaComponent},   
    {path: 'footer', component: FooterComponent},

    {path: 'bioskop', component: BioskopComponent},
    {path: 'filmovi/:id', component: FilmoviComponent},
    {path:'postfilmovi', component: PostfilmoviComponent},

    {path: 'izleti', component: IzletiComponent},
    {path: 'izletiid/:id', component: IzletiidComponent},
    {path: 'postizleti', component: PostizletiComponent},

    {path: 'pozoriste', component: PozoristeComponent},
    {path: 'pozoristeid/:id', component: PozoristeidComponent},
    {path: 'postpozoriste', component: PostpozoristeComponent},

    {path: 'izlozbe', component: IzlozbeComponent},
    {path: 'izlozba/:id', component: IzlozbaComponent},
    {path: 'postizlozba', component: PostizlozbaComponent},

    {path: 'nightlife', component: NightlifeComponent},
    {path: 'nlid/:id', component: NlidComponent},
    {path: 'postnl', component: PostnlComponent},

    {path: 'klub-citalaca', component: KlubCitalacaComponent},
    {path: 'klubcid/:id', component: KlubcidComponent},
    {path: 'postklubc', component: PostklubcComponent},


    {path: 'kontakt', component: KontaktComponent},
    {path: 'logout', component: LogoutComponent},
    {path: 'login', component: LoginComponent},

];
@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],

    exports: [RouterModule]
})

export class AppRoutingModule {

}