import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-pretraga-dogadjaja',
  templateUrl: './pretraga-dogadjaja.component.html',
  styleUrls: ['./pretraga-dogadjaja.component.css']
})
export class PretragaDogadjajaComponent implements OnInit {
  filmovi: any [];
  izleti: any[];
  pozoriste: any[];
  izlozbe: any[];
  klubc: any[];
  nightlife: any[]; 
  
  dan = 86400000;
  date1: Date = new Date();
  date2: Date = new Date();
  dog: string;
  format: string[] = ['dd.MM.yyyy.']
  settings = {
      bigBanner: true,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  filter (dal: boolean) {
    let time1 = new Date(this.date1)
    let time2 = new Date(this.date2)

    this.http.get('api/naslovi').subscribe(result => {
      this.filmovi = result as any[];
      if (dal) this.filmovi = this.filmovi.filter(x =>  x.datum  <=  time2.getTime() + this.dan && x.datum  >= time1.getTime() - this.dan)
      console.log(this.filmovi)
    }, error => console.error(error));
    
    this.http.get('api/izleti').subscribe(result => {
      this.izleti = result as any[];
      if (dal) this.izleti = this.izleti.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    }, error => console.error(error));
  
    this.http.get('api/izlozbe').subscribe(result => {
      this.izlozbe = result as any[];
      if (dal) this.izlozbe = this.izlozbe.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    }, error => console.error(error));
  
    this.http.get('api/citalac').subscribe(result => {
      this.klubc = result as any[];
      if (dal) this.klubc = this.klubc.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time2.getTime() - this.dan)
    }, error => console.error(error));
  
    this.http.get('api/nightlife').subscribe(result => {
      this.nightlife = result as any[];
      if (dal) this.nightlife = this.nightlife.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    }, error => console.error(error));
  
    this.http.get('api/repertoar').subscribe(result => {
      this.pozoriste = result as any[];
      if (dal) this.pozoriste = this.pozoriste.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    }, error => console.error(error));
  }
constructor(private http: HttpClient) {
  this.filmovi = [] as any[];
  this.dog = "sve"; 
  this.filter(false);
}

  submit() { //svaki filtriram
    let time1 = new Date(this.date1)
    let time2 = new Date(this.date2)

console.log(this.dog)

  if (this.dog == 'sve')
  { 
    this.filter(true);
  
  }else if(this.dog == 'izleti'){
    this.http.get('api/izleti').subscribe(result => {
      this.izleti = result as any[];
    this.izleti = this.izleti.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    this.filmovi = [];
    this.izlozbe = [];
    this.klubc = [];
    this.nightlife = [];
    this.pozoriste = [];
  }, error => console.error(error));

  } else if(this.dog == 'filmovi'){
    this.http.get('api/naslovi').subscribe(result => {
      this.filmovi = result as any[];
    this.filmovi = this.filmovi.filter(x =>  x.datum  <=  time2.getTime() + this.dan && x.datum  >= time1.getTime() - this.dan)
    this.izlozbe = [];
    this.klubc = [];
    this.nightlife = [];
    this.pozoriste = [];
    this.izleti = [];
    }, error => console.error(error));
    

  } else if(this.dog == 'izlozbe'){
    this.http.get('api/izlozbe').subscribe(result => {
      this.izlozbe = result as any[];
    this.izlozbe = this.izlozbe.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    this.filmovi = [];
    this.klubc = [];
    this.nightlife = [];
    this.pozoriste = [];
    this.izleti = [];
    }, error => console.error(error));
  }
  else if(this.dog == 'klubc'){
    this.http.get('api/citalac').subscribe(result => {
      this.klubc = result as any[];
    this.klubc = this.klubc.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time2.getTime() - this.dan)
    this.filmovi = [];
    this.izlozbe = [];
    this.nightlife = [];
    this.pozoriste = [];
    this.izleti = [];
    }, error => console.error(error));
  }
  else if(this.dog == 'nightlife'){
    this.http.get('api/nightlife').subscribe(result => {
      this.nightlife = result as any[];
    this.nightlife = this.nightlife.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    this.filmovi = [];
    this.klubc = [];
    this.izlozbe = [];
    this.pozoriste = [];
    this.izleti = [];
    }, error => console.error(error));
  }
  else if(this.dog == 'pozoriste'){
    this.http.get('api/repertoar').subscribe(result => {
      this.pozoriste = result as any[];
    this.pozoriste = this.pozoriste.filter(x => x.datum <=  time2.getTime() + this.dan && x.datum >= time1.getTime() - this.dan)
    this.filmovi = [];
    this.klubc = [];
    this.izlozbe = [];
    this.nightlife = [];
    this.izleti = [];
    console.log(this.pozoriste)
    }, error => console.error(error));
  }
}

ngOnInit() {}

}
