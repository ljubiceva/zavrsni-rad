import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-unos-dogadjaja',
  templateUrl: './unos-dogadjaja.component.html',
  styleUrls: ['./unos-dogadjaja.component.css']
})
export class UnosDogadjajaComponent implements OnInit {
  title = 'ar-dogadjaji';
  route: any;
  naziv = '';
  opis = '';
  slika = '';
  imdb = '';
  trejler = '';
  opisId = '';
  o = {};
  
  constructor(private http: HttpClient) {}

  OnSacuvaj() {
    console.log(this.naziv)
    console.log(this.opis)
    console.log(this.slika)
    console.log(this.imdb)
    console.log(this.trejler)
    console.log(this.opisId)



    var filmovi = {
      naziv: this.naziv,
      opis: this.opis,
      slika: this.slika,
      imdb: this.imdb,
      trejler: this.trejler,
      opisId: this.opisId
    }

    this.http.post('api/postfilmovi', filmovi).subscribe((response: Response) => response.json())
      , error => console.error(error);
  }

  ngOnInit() {
  }

}