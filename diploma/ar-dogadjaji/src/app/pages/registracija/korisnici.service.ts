import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

@Injectable()
export class KorisniciService {
    constructor(private http: Http) {}
    storeKorisnici(korisnici: any[]) {
      return this.http.post('', korisnici);

    }
    getKorisnici() {
      return this.http.get('');

    }
}
