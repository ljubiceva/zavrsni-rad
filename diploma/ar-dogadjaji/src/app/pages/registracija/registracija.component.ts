import { Component, OnInit } from '@angular/core';
import { KorisniciService } from './korisnici.service';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.css']
})
export class RegistracijaComponent implements OnInit {
  ime: '';
  prezime: '';
  email: '';
  password: '';

korisnici = [
  
];

  ngOnInit() {
  }

  OnSacuvaj(name: string) {
    this.korisnici.push({
      ime: this.ime,
      email: this.email,
      prezime: this.prezime,
      password: this.password,
      capacity: 50,
      id: this.generateId()
    });
  }
  onSacuvajK(){
    this.korisniciService.storeKorisnici(this.korisnici)
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );
  }
 
  constructor(private korisniciService: KorisniciService) {}
  private generateId() {
    return Math.round(Math.random() * 1000);
  }
}
