import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ComponentsModule } from '../components/components.module';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';


import { UnosDogadjajaComponent } from './unos-dogadjaja/unos-dogadjaja.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PretragaDogadjajaComponent } from './pretraga-dogadjaja/pretraga-dogadjaja.component';
import { RegistracijaComponent } from './registracija/registracija.component';
import { LogoutComponent } from './login/logout/logout.component';

import { KorisniciService } from './registracija/korisnici.service';
import { KontaktComponent } from './kontakt/kontakt.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';




@NgModule({
  declarations: [
      UnosDogadjajaComponent,
      HomeComponent,
      LoginComponent,
      PretragaDogadjajaComponent,
      RegistracijaComponent,
      LogoutComponent,
      KontaktComponent,
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ComponentsModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AngularDateTimePickerModule,
    

  ],

  exports: [
    UnosDogadjajaComponent,
    HomeComponent,
    LoginComponent,
    PretragaDogadjajaComponent,
    RegistracijaComponent,
    LogoutComponent,
    KontaktComponent,
    
  ],

  providers: [KorisniciService],
  bootstrap: []
})
export class PagesModule { }
