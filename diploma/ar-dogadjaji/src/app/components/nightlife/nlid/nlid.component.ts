import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-nlid',
  templateUrl: './nlid.component.html',
  styleUrls: ['./nlid.component.css']
})
export class NlidComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;
  myTime: Date = new Date(); 
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  isMeridian = false;
  showSpinners = true;
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
  confirm(): void {
    this.modalRef.hide();
  }
  decline(): void {
    this.modalRef.hide();
  }
  title = 'ar-dogadjaji';
    public nightlife: any [];
    route: any;
    nl: any;
    params: any;
    constructor(private http: HttpClient,private modalService: BsModalService, private r: ActivatedRoute, private _sanitizer: DomSanitizer,private router: Router){
    this.nightlife = []
    }
  


  ngOnInit() {

    this.r.params.subscribe(params => {
       this.http.get('api/nightlife/' + params["id"]).subscribe(result => {
          this.nightlife = result as any [];
          this.nl = this.nightlife[0] 
       }, error => console.error(error));
      })
  }
  onIzbrisinl(){

    this.r.params.subscribe(params => {
     this.http.delete('api/nightlife/izbrisinl/' + params["id"]).subscribe(result => {
        this.nightlife = result as any [];
        this.nl = this.nightlife[0] 
        
     }, error => console.error(error));
    })

  }
 safeURL(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  } 
  }


