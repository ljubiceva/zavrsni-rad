import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nightlife',
  templateUrl: './nightlife.component.html',
  styleUrls: ['./nightlife.component.css']
})
export class NightlifeComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']
  
  title = 'ar-dogadjaji';
  public nightlife: any [];
  constructor(private http: HttpClient, private r: ActivatedRoute) {
    this.http.get('api/nightlife').subscribe(result => {
      this.nightlife = result as any[];
    }, error => console.error(error));
   }

  ngOnInit() {
  }

}
