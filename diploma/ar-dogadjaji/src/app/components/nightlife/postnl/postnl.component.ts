import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-postnl',
  templateUrl: './postnl.component.html',
  styleUrls: ['./postnl.component.css']
})
export class PostnlComponent implements OnInit {
  myTime: Date = new Date(); 
  isMeridian = false;
  showSpinners = true;
  modalRef: BsModalRef;
  message: string;

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    this.modalRef.hide();
    this.naziv = '';
    this.opis = '';
    this.slika = '';
    this.trejler = '';
    this.lokacija = '';
    this.trejler = '';
    this.opisId = '';
    this.datum = '';
    this.myTime = null;

  }

  title = 'ar-dogadjaji';
  vreme: Date = new Date();
  
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }
  route: any;
  naziv = '';
  opis = '';
  slika = '';
  trejler = '';
  lokacija = '';
  opisId = '';
  datum = '';
  nl: any;
  constructor(private http: HttpClient, private modalService: BsModalService) {
  }


  OnSacuvaj() {

    let time = new Date(this.vreme)


    var nl = {
      naziv: this.naziv,
      opis: this.opis,
      slika: this.slika,
      trejler: this.trejler,
      lokacija: this.lokacija,
      opisId: this.opisId,
      datum: time.getTime(),
      vreme: time.getTime(),
      myTime: this.myTime

    };



    return this.http.post('api/postnl', nl).subscribe(result => {
      this.nl = result as any[];
      console.log(this.nl)
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
