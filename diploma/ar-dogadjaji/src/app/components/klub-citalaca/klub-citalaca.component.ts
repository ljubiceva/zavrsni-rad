import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-klub-citalaca',
  templateUrl: './klub-citalaca.component.html',
  styleUrls: ['./klub-citalaca.component.css']
})
export class KlubCitalacaComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  title = 'ar-dogadjaji';
  public klubc: any [];
  constructor(private http: HttpClient, private r: ActivatedRoute){
    this.http.get('api/citalac').subscribe(result => {
      this.klubc = result as any[];
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
