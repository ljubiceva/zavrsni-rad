import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-klubcid',
  templateUrl: './klubcid.component.html',
  styleUrls: ['./klubcid.component.css']
})
export class KlubcidComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;

  myTime: Date = new Date(); 
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  isMeridian = false;
  showSpinners = true;

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
  confirm(): void {
    this.modalRef.hide();
  }
  decline(): void {
    this.modalRef.hide();
  }
  title = 'ar-dogadjaji';
  public klubc: any[];
  route: any;
  klub: any;
  params: any;

  constructor(private http: HttpClient, private modalService: BsModalService, private r: ActivatedRoute, private _sanitizer: DomSanitizer, private router: Router) {
    this.klubc = []
  }

  ngOnInit() {

    this.r.params.subscribe(params => {
      this.http.get('api/citalac/' + params["id"]).subscribe(result => {
        this.klubc = result as any[];
        this.klub = this.klubc[0]
      }, error => console.error(error));
    })
  }
  onIzbrisikc() {

    this.r.params.subscribe(params => {
      this.http.delete('api/citalac/izbrisikc/' + params["id"]).subscribe(result => {
        this.klubc = result as any[];
        this.klub = this.klubc[0]

      }, error => console.error(error));
    })
  }
  safeURL(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}


