import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bioskop',
  templateUrl: './bioskop.component.html',
  styleUrls: ['./bioskop.component.css']
})
export class BioskopComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  public filmovi: any[];
  constructor(private http: HttpClient, private r: ActivatedRoute) {
    this.http.get('api/naslovi').subscribe(result => {
      this.filmovi = result as any[];
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
