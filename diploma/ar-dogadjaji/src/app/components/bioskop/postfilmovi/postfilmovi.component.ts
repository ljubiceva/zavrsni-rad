import { Component, OnInit, TemplateRef  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-postfilmovi',
  templateUrl: './postfilmovi.component.html' ,
  styleUrls: ['./postfilmovi.component.css']
})
export class PostfilmoviComponent implements OnInit {
  myTime: Date = new Date(); 
  isMeridian = false;
  showSpinners = true;
  modalRef: BsModalRef;
  message: string;

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
 
  confirm(): void {
    this.modalRef.hide();
    this.naziv = '';
    this.opis  = '';
    this.slika = '';
    this.lokacija = '';
    this.zanr = '';
    this.imdb = '';
    this.trejler = '';
    this.kritika = '';
    this.opisId = '';
    this.datum = '';
    this.myTime = null;
  }
 
  title = 'ar-dogadjaji';
  vreme: Date = new Date();
  settings = {
      bigBanner: true,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  route: any;
  naziv = '';
  opis  = '';
  slika = '';
  lokacija = '';
  zanr = '';
  imdb = '';
  trejler = '';
  kritika = '';
  opisId = '';
  datum = '';
  filmovi: any;
  constructor(private http: HttpClient, private modalService: BsModalService) {
  }
  

  
  OnSacuvaj() {
    console.log(this.naziv)
    console.log(this.opis)   
    console.log(this.slika)
    console.log(this.zanr)
    console.log(this.imdb)
    console.log(this.trejler)
    console.log(this.kritika)
    console.log(this.opisId)
    console.log(this.datum)
    console.log(this.myTime)

 
    let time = new Date(this.vreme)


    var filmovi =  {
      naziv: this.naziv,
      opis: this.opis,
      slika: this.slika,
      lokacija: this.lokacija,
      zanr: this.zanr,
      imdb: this.imdb,
      trejler: this.trejler,
      kritika: this.kritika,
      opisId: this.opisId,
      datum: time.getTime(),
      vreme: time.getTime(),
      myTime: this.myTime
      
    };
   

    return this.http.post('api/postfilmovi', filmovi).subscribe(result => {
      this.filmovi = result as any[];
      console.log(this.filmovi)
      }, error => console.error(error));
      
      
    }

  ngOnInit() {
  }

}
