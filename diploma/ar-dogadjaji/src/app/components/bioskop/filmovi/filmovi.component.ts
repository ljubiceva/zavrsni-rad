import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-filmovi',
  templateUrl: './filmovi.component.html',
  styleUrls: ['./filmovi.component.css']
})
export class FilmoviComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;
  
  myTime: Date = new Date(); 
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  isMeridian = false;
  showSpinners = true;


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
  confirm(): void {
    this.modalRef.hide();
  }
  decline(): void {
    this.modalRef.hide();
  }
  title = 'ar-dogadjaji';
  public filmovi: any[];
  route: any;
  film: any;
  params: any;
  constructor(private http: HttpClient, private r: ActivatedRoute, private _sanitizer: DomSanitizer, private router: Router, private modalService: BsModalService) {
    this.filmovi = []
  }

  ngOnInit() {

    this.r.params.subscribe(params => {
      this.http.get('api/naslovi/' + params["id"]).subscribe(result => {
        this.filmovi = result as any[];
        this.film = this.filmovi[0]

      }, error => console.error(error));
    })
  }
  onIzbrisif() {

    this.r.params.subscribe(params => {
      this.http.delete('api/naslovi/izbrisi/' + params["id"]).subscribe(result => {
        this.filmovi = result as any[];
        this.film = this.filmovi[0]

      }, error => console.error(error));
    })
  }


  safeURL(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  safeURLimdb(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}


