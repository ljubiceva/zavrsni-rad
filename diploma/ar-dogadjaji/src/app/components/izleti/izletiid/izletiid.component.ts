import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-izletiid',
  templateUrl: './izletiid.component.html',
  styleUrls: ['./izletiid.component.css']
})
export class IzletiidComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;
  myTime: Date = new Date(); 
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  isMeridian = false;
  showSpinners = true;

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
  confirm(): void {
    this.modalRef.hide();
  }
  decline(): void {
    this.modalRef.hide();
  }
  title = 'ar-dogadjaji';
  public izleti: any[];
  route: any;
  izlet: any;
  params: any;
  constructor(private http: HttpClient, private modalService: BsModalService, private r: ActivatedRoute, private _sanitizer: DomSanitizer, private router: Router) {
    this.izleti = []
  }



  ngOnInit() {

    this.r.params.subscribe(params => {
      this.http.get('api/izleti/' + params["id"]).subscribe(result => {
        this.izleti = result as any[];
        this.izlet = this.izleti[0]
      }, error => console.error(error));
    })
  }

  onIzbrisii() {

    this.r.params.subscribe(params => {
      this.http.delete('api/izleti/izbrisii/' + params["id"]).subscribe(result => {
        this.izleti = result as any[];
        this.izlet = this.izleti[0]
      }, error => console.error(error));
    })
  }
  safeURL(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}


