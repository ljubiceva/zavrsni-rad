import { Component, OnInit,TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-postizleti',
  templateUrl: './postizleti.component.html',
  styleUrls: ['./postizleti.component.css']
})
export class PostizletiComponent implements OnInit {
  myTime: Date = new Date(); 
  isMeridian = false;
  showSpinners = true;
  modalRef: BsModalRef;
  message: string;
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
 
  confirm(): void {
    this.modalRef.hide();
    this.naziv = '';
    this.opis  = '';
    this.slika = '';
    this.lokacija = '';
    this.trejler = '';
    this.opisId = '';
    this.datum = '';
    this.myTime = null;
  }
  title = 'ar-dogadjaji';
  vreme: Date = new Date();
  settings = {
      bigBanner: true,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  route: any;
  naziv = '';
  opis  = '';
  slika = '';
  lokacija = '';
  trejler = '';
  opisId = '';
  datum = '';
  izleti: any;
  constructor(private http: HttpClient, private modalService: BsModalService) {
  }


  OnSacuvaj() {
    console.log(this.naziv)
    console.log(this.opis)   
    console.log(this.slika)
    console.log(this.trejler)
    console.log(this.opisId)
 
    let time = new Date(this.vreme)


    var izleti =  {
      naziv: this.naziv,
      opis: this.opis,
      slika: this.slika,
      lokacija: this.lokacija,
      trejler: this.trejler,
      opisId: this.opisId,
      datum: time.getTime(),
      vreme: time.getTime(),
      myTime: this.myTime
    };

    

    return this.http.post('api/postizleti', izleti).subscribe(result => {
      this.izleti = result as any[];
      console.log(this.izleti)
      }, error => console.error(error));
    }

  ngOnInit() {
  }

}
