import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-izleti',
  templateUrl: './izleti.component.html',
  styleUrls: ['./izleti.component.css']
})
export class IzletiComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']
  
  title = 'ar-dogadjaji';
  public izleti: any [];
  constructor(private http: HttpClient, private r: ActivatedRoute){
    this.http.get('api/izleti').subscribe(result => {
      this.izleti = result as any[];
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
