import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pozoriste',
  templateUrl: './pozoriste.component.html',
  styleUrls: ['./pozoriste.component.css']
})
export class PozoristeComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']
  title = 'ar-dogadjaji';
  public pozoriste: any [];
  constructor(private http: HttpClient, private r: ActivatedRoute){
    this.http.get('api/repertoar').subscribe(result => {
      this.pozoriste = result as any[];
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
