import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-pozoristeid',
  templateUrl: './pozoristeid.component.html',
  styleUrls: ['./pozoristeid.component.css']
})
export class PozoristeidComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;
  myTime: Date = new Date(); 
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  isMeridian = false;
  showSpinners = true;
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
  confirm(): void {
    this.modalRef.hide();
  }
  decline(): void {
    this.modalRef.hide();
  }
  title = 'ar-dogadjaji';
  public pozoriste: any[];
  route: any;
  pozorista: any;
  params: any;

  constructor(private http: HttpClient, private r: ActivatedRoute, private _sanitizer: DomSanitizer, private router: Router, private modalService: BsModalService) {
    this.pozoriste = []
  }



  ngOnInit() {

    this.r.params.subscribe(params => {
      this.http.get('api/repertoar/' + params["id"]).subscribe(result => {
        this.pozoriste = result as any[];
        this.pozorista = this.pozoriste[0]
      }, error => console.error(error));
    })
  }

  onIzbrisip() {

    this.r.params.subscribe(params => {
      this.http.delete('api/repertoar/izbrisip/' + params["id"]).subscribe(result => {
        this.pozoriste = result as any[];
        this.pozorista = this.pozoriste[0]
      }, error => console.error(error));
    })
  }


  safeURL(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}


