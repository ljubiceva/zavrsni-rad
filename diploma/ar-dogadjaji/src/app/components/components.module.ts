import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { HeaderComponent } from './header/header.component';
import { IzletiComponent } from './izleti/izleti.component';
import { IzlozbeComponent } from './izlozbe/izlozbe.component';
import { KlubCitalacaComponent } from './klub-citalaca/klub-citalaca.component';
import { NightlifeComponent } from './nightlife/nightlife.component';
import { PozoristeComponent } from './pozoriste/pozoriste.component';
import { BioskopComponent } from './bioskop/bioskop.component';
import { FooterComponent } from './footer/footer.component';
import { IzletiidComponent } from './izleti/izletiid/izletiid.component';
import { PozoristeidComponent } from './pozoriste/pozoristeid/pozoristeid.component';
import { IzlozbaComponent } from './izlozbe/izlozba/izlozba.component';
import { NlidComponent } from './nightlife/nlid/nlid.component';
import { KlubcidComponent } from './klub-citalaca/klubcid/klubcid.component';
import { PostfilmoviComponent } from './bioskop/postfilmovi/postfilmovi.component';
import { PostizletiComponent } from './izleti/postizleti/postizleti.component';
import { PostizlozbaComponent } from './izlozbe/postizlozba/postizlozba.component';
import { PostklubcComponent } from './klub-citalaca/postklubc/postklubc.component';
import { PostnlComponent } from './nightlife/postnl/postnl.component';
import { PostpozoristeComponent } from './pozoriste/postpozoriste/postpozoriste.component';
import { KorisniciService } from '../pages/registracija/korisnici.service';
import { FilmoviComponent } from './bioskop/filmovi/filmovi.component';

import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { NgbModal, NgbModalConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  declarations: [
    HeaderComponent,
    IzletiComponent,
    IzlozbeComponent,
    KlubCitalacaComponent,
    NightlifeComponent,
    PozoristeComponent,
    BioskopComponent ,
    FooterComponent,
    FilmoviComponent,
    IzletiidComponent,
    PozoristeidComponent,
    IzlozbaComponent,
    NlidComponent,
    KlubcidComponent,
    PostfilmoviComponent,
    PostizletiComponent,
    PostizlozbaComponent,
    PostizlozbaComponent,
    PostklubcComponent,
    PostklubcComponent,
    PostnlComponent,
    PostpozoristeComponent,
  ],

  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    AngularDateTimePickerModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    TimepickerModule.forRoot(),
    CarouselModule.forRoot()

   
  ],
  exports: [
    HeaderComponent,
    IzletiComponent,
    IzlozbeComponent,
    KlubCitalacaComponent,
    NightlifeComponent,
    PozoristeComponent,
    BioskopComponent,
    FooterComponent,
    IzletiidComponent,
    PozoristeidComponent,
    IzlozbaComponent,
    NlidComponent,
    KlubcidComponent,
    PostfilmoviComponent,
    PostizletiComponent,
    PostizlozbaComponent,
    PostklubcComponent,
    PostnlComponent,
    PostpozoristeComponent,

  ],
 

  providers: [KorisniciService, NgbModalConfig, NgbModal ],
  bootstrap: []
})
export class ComponentsModule { }
