import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-izlozbe',
  templateUrl: './izlozbe.component.html',
  styleUrls: ['./izlozbe.component.css']
})
export class IzlozbeComponent implements OnInit {
  myTime: Date = new Date();
  format1: string[] = ['HH:mm']
  format2: string[] = ['dd.MM.yyyy.']

  title = 'ar-dogadjaji';
  public izlozbe: any [];
  constructor(private http: HttpClient, private r: ActivatedRoute){
    this.http.get('api/izlozbe').subscribe(result => {
      this.izlozbe = result as any[];
    }, error => console.error(error));
  }


  ngOnInit() {
  }

}
