import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-postizlozba',
  templateUrl: './postizlozba.component.html',
  styleUrls: ['./postizlozba.component.css']
})
export class PostizlozbaComponent implements OnInit {
  myTime: Date = new Date(); 
  isMeridian = false;
  showSpinners = true;

  modalRef: BsModalRef;
  message: string;
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
 
  confirm(): void {
    this.modalRef.hide();
    this.naziv = '';
    this.opis  = '';
    this.slika = '';
    this.slika2 = '';
    this.slika3 = '';
    this.lokacija = '';
    this.trejler = '';
    this.opisId = '';
    this.datum = '';
    this.myTime = null;

  }
  
  title = 'ar-dogadjaji';
  vreme: Date = new Date();
  settings = {
      bigBanner: true,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  route: any;
  naziv = '';
  opis  = '';
  slika = '';
  slika1 = '';
  slika2 = '';
  slika3 = '';
  lokacija = '';
  trejler = '';
  opisId = '';
  datum = '';
  izlozbe: any;
  constructor(private http: HttpClient, private modalService: BsModalService) {
  }


  OnSacuvaj() {
    console.log(this.naziv)
    console.log(this.opis)   
    console.log(this.slika)
    console.log(this.trejler)
    console.log(this.opisId)

    let time = new Date(this.vreme)
 


    var izlozbe =  {
      naziv: this.naziv,
      opis: this.opis,
      slika: this.slika,
      slika1: this.slika1,
      slika2: this.slika2,
      slika3: this.slika3,
      lokacija: this.lokacija,
      trejler: this.trejler,
      opisId: this.opisId,
      datum: time.getTime(),
      vreme: time.getTime(),
      myTime: this.myTime

    };

    

    return this.http.post('api/postizlozba', izlozbe).subscribe(result => {
      this.izlozbe = result as any[];
      console.log(this.izlozbe)
      }, error => console.error(error));
    }

  ngOnInit() {
  }

}
