import { Component, OnInit,TemplateRef } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
    selector: 'app-izlozba',
    templateUrl: './izlozba.component.html',
    styleUrls: ['./izlozba.component.css']
})
export class IzlozbaComponent implements OnInit {
    modalRef: BsModalRef;
    message: string;
    
    myTime: Date = new Date(); 
    format1: string[] = ['HH:mm']
    format2: string[] = ['dd.MM.yyyy.']
  
    isMeridian = false;
    showSpinners = true;

    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
    }
    confirm(): void {
      this.modalRef.hide();
    }
    decline(): void {
      this.modalRef.hide();
    }
    title = 'ar-dogadjaji';
    public izlozbe: any[];
    route: any;
    izlozba: any;
    params: any;
    constructor(private http: HttpClient,private modalService: BsModalService, private r: ActivatedRoute, private _sanitazer: DomSanitizer, private router: Router) {
        this.izlozbe = []
    }



    ngOnInit() {
        this.r.params.subscribe(params => {
            this.http.get('api/izlozbe/' + params["id"]).subscribe(result => {
                this.izlozbe = result as any[];
                this.izlozba = this.izlozbe[0]
            }, error => console.error(error));
        })
    }
    
    onIzbrisiizb(){

        this.r.params.subscribe(params => {
         this.http.delete('api/izlozbe/izbrisiizb/' + params["id"]).subscribe(result => {
            this.izlozbe = result as any [];
            this.izlozba = this.izlozbe[0] 
            
         }, error => console.error(error));
        })
      }
}

