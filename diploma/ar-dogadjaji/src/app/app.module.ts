import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { ComponentsModule } from './components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { AppComponent } from './app.component';
// import { AuthService } from './auth.service';
// import { AuthGuard } from './auth-guard.service';
import { KorisniciService } from './pages/registracija/korisnici.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    PagesModule,
    ComponentsModule,
    AppRoutingModule,
    HttpModule,
    AngularDateTimePickerModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),

  ],
  exports: [NgbModule],
 
  providers: [
    // AuthService, AuthGuard, 
    KorisniciService],
  bootstrap: [AppComponent]
})
export class AppModule { }
