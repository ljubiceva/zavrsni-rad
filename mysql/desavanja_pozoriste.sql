-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pozoriste`
--

DROP TABLE IF EXISTS `pozoriste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pozoriste` (
  `idpozoriste` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `opis` varchar(600) NOT NULL,
  `slika` varchar(450) NOT NULL,
  `trejler` varchar(450) NOT NULL,
  `opisid` varchar(450) NOT NULL,
  `datum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpozoriste`,`naziv`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pozoriste`
--

LOCK TABLES `pozoriste` WRITE;
/*!40000 ALTER TABLE `pozoriste` DISABLE KEYS */;
INSERT INTO `pozoriste` VALUES (1,'Bela kafa','Junaci ove predstave su sazdani od protivrečnosti; oni se i vole i mrze, uvažavaju i preziru, beže jedni od drugih da bi se željno vraćali, oni su vernici i nevernici, pouzdani i nesigurni, rečju, oni su sastavljeni od vrlina i nedostataka običnih ljudi. Uistinu, Popovićevi junaci pate i kada su radosni, a smeju se kada ih život šiba.','https://www.narodnopozoriste.rs/media/images/performances/7456/thumb/plakat.jpg','https://www.youtube.com/embed/MJc2r4JHPBI','op','1549987481000'),(2,'Konstelacije','KONSTELACIJE su drama koja pokazuje delovanje ove teorije u praksi, kroz odnos dvoje ljudi u paralelnim univerzumima. Komad istražuje slobodnu volju i ulogu slučajnosti u našim životima.','https://atelje212.rs/wp-content/uploads/2016/03/aca3-DEFFINAL-PLAKAT-01.jpg','https://www.youtube.com/embed/g-KeanoN__Q','op','1550230304000'),(3,'Seviljski berberin','Rozina, mlada devojka, zaljubljena je u grofa Almavivu, mladog i lepog, koji peva pod njenim prozorom želeći da joj se približi. Ali, Rozinu čuva stari doktor Bartolo, koji želi da se njome oženi, i ne dozvoljava joj da izlazi iz kuće. Figaro, lični berberin Bartolov, pomaže mladima da se upoznaju, predstavljajući Almavivu kao učitelja pevanja. I dok Figaro brije Bartola, Rozina i Almamiva se dogovaraju o svojoj budućnosti.','https://hocupozoriste.rs/slike/predstave/seviljski-berberin-narodno-bg.jpg','https://www.youtube.com/embed/G1ywafDgDXQ','d','1549814290000');
/*!40000 ALTER TABLE `pozoriste` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:31
