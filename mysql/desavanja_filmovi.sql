-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `filmovi`
--

DROP TABLE IF EXISTS `filmovi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `filmovi` (
  `idfilmovi` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `opis` varchar(500) NOT NULL,
  `slika` varchar(500) NOT NULL,
  `zanr` varchar(100) NOT NULL,
  `imdb` varchar(500) NOT NULL,
  `trejler` varchar(500) NOT NULL,
  `kritika` varchar(450) NOT NULL,
  `opisid` varchar(1000) NOT NULL,
  `datum` varchar(100) NOT NULL,
  `myTime` varchar(450) NOT NULL,
  PRIMARY KEY (`idfilmovi`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filmovi`
--

LOCK TABLES `filmovi` WRITE;
/*!40000 ALTER TABLE `filmovi` DISABLE KEYS */;
INSERT INTO `filmovi` VALUES (1,'Schindler\'s List (1993)','“Šindlerova lista” - dramatična ekranizacija mračnog i zastrašujućeg perioda za vreme Drugog svetskog rata. Dirljiva i potresna ali i istinita priča koja odaje počasti svima onima koji su izgubili živote u Holokaustu.','https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg','Biography, Drama, History ','https://www.imdb.com/title/tt0108052/','https://www.youtube.com/embed/mxphAlJID9U','https://www.rottentomatoes.com/m/schindlers_list','Schindlerova lista - američka epska drama iz 1993. godine snimljena po knjizi  \"Schindler\'s Ark\" australskog autora Thomasa Keneallyja. Radnja filma govori o Oskaru Schindleru, nemačkom biznismenu koji je spasio živote više od hiljadu uglavnom poljskih Jevreja tokom Holokausta omogućivši im da rade u njegovim fabrikama. U filmu su glavne uloge ostvarili Liam Neeson kao Schindler, Ralph Fiennes kao Amon Göth i Ben Kingsley kao Schindlerov jevrejski knjigovođa Itzhak Stern.','1550230304000','2019-02-05T18:30:44.264Z'),(2,'Beautiful mind (2001)','Biografska drama o životu i radu zaslužnog matematičara Johna Nash-a, po knjizi Sylvije Nasar, \"Genijalni um\" prikazuje sve kompleksne reakcije ljudskog uma na mentalne bolesti.','https://resizing.flixster.com/6Ux4qpxHFGUcpcq_8ghFi70JZO8=/206x305/v1.bTsxMTYxNDIzMztqOzE4MDQ0OzEyMDA7Nzk4OzEwNjQ','Drama, Biography','https://www.imdb.com/title/tt0268978/','https://www.youtube.com/embed/9wZM7CQY130','https://www.rottentomatoes.com/m/beautiful_mind','\"Genijalni um\" je brižljivo i precizno ispričana umalo tragična priča. Gradeći film oko izvrsnog Russella Crowea u glavnoj ulozi \'\'ludoga genija\'\', Howard je pokazao kako vrlo dobro poznaje mehanizme uključivanja i angažovanja publike, koji su mu onda i omogućili uverljivost riskantnih scena paralelnog sveta unutar Nashevog uma. Prikazujući sve kompleksne reakcije ljudskog uma na mentalne bolesti, Howard i Crowe su uspeli da naglase ponos takvog pacijenta. Uz briljantnog Crowea, film su svojim nastupima obogatili i, među ostalima, Ed Harris i Christopher Plummer, a posebno treba istaknuti jednu od najboljih glumica svoje generacije, Jennifer Connelly.','1549814290000','2019-02-05T19:00:10.265Z'),(3,'Shutter island (2008)','Zatvoreno ostrvo (engl. Shutter Island) je psihološki triler Martina Skorsezea iz 2010. sa Leonardom Dikapriom u glavnoj ulozi. ','https://images-na.ssl-images-amazon.com/images/I/51cJfE3YuML._SY445_.jpg','Action & Adventure, Drama, Mystery & Suspense','https://www.imdb.com/title/tt1130884/?ref_=nv_sr_1','https://www.youtube.com/embed/5iaYLCiq5RM','https://www.rottentomatoes.com/m/1198124_shutter_island','Zatvoreno ostrvo (engl. Shutter Island) je psihološki triler Martina Skorsezea iz 2010. sa Leonardom Dikapriom u glavnoj ulozi. Film je snimljen po istoimenom romanu Denisa Lehejna (engl. Dennis Lehane). Pripreme za film su započete u martu 2008. i bilo je planirano da premijera bude krajem 2009, ali su se snimanje i postprodukcija, usled ekonomske krize, odužili do 19. februara 2010.','1549987481000','2019-02-05T16:00:41.556Z'),(4,'Exam (2009)','Ispit je britanski triler nastao 2009. godine. Motivisani novim tehnologijama i napretkom u svetu informatike, producenti ovog filma napravili su ga sa dozom neizvesnosti i neocekivanim ishodom.','https://resizing.flixster.com/WPA9z3HUfiMBG0jobmW1_JL8fSU=/206x305/v1.bTsxMTIzOTMwNTtqOzE3OTk1OzEyMDA7MTQ3OTsxOTcy','Horror, Mystery & Suspense, Science Fiction & Fantasy','https://www.imdb.com/title/tt1258197/','https://www.youtube.com/embed/bkdt2Sygew0','https://www.rottentomatoes.com/m/exam_2008','Radnja filma smeštena je u Velikoj Britaniji, gde je zavladala bolest, tačnije virus, i stanovništvo je zavisno od lekova. Moraju ih uzimati na nekoliko sati, kako ne bi pali u komu, nakon čega bi usledila smrt. Radnja filma se odvija u jednoj prostoriji gde dolazi osam kandidata da polažu ispit kako bi zaslužili važan posao kao lični asistent generalnog direktora kompanije koja će pronaći lek protiv ove bolesti.  ','1551283551000','2019-02-05T20:00:51.446Z');
/*!40000 ALTER TABLE `filmovi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:29
