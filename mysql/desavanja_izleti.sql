-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `izleti`
--

DROP TABLE IF EXISTS `izleti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `izleti` (
  `idizleti` int(11) NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(45) NOT NULL,
  `Opis` varchar(500) NOT NULL,
  `Slika` varchar(200) NOT NULL,
  `Trejler` varchar(500) DEFAULT NULL,
  `OpisId` varchar(450) NOT NULL,
  `datum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idizleti`),
  UNIQUE KEY `idizleti_UNIQUE` (`idizleti`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izleti`
--

LOCK TABLES `izleti` WRITE;
/*!40000 ALTER TABLE `izleti` DISABLE KEYS */;
INSERT INTO `izleti` VALUES (1,'Bukulja','Bukulja je planina u Šumadiji, u čijem podnožju se nalaze Aranđelovac i Bukovička banja, a njen najviši vrh je visok 696 metara. Pogodna je za pripreme sportskih ekipa, a obeležene pešačke staze vode od parka Bukovičke banje do vrha planine.','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBP8qSA5PH2U3Pe7_mo4yWq1_vBCzgXIDIx7JbrksBFpKNtG7r','https://www.youtube.com/embed/JGmiuCjuwnY','wer','1550230304000'),(2,'Bukovička banja','U samom gradu nalazi se Bukovička banja, poznato banjsko i klimatsko lečilište, čija mineralna voda ima lekovita svojstva i to kod oboljenja i stanja hepatobilijarnog trakta, gastrointerestinalnog trakta i dijabetesa.','http://www.panacomp.net/wp-content/uploads/2017/05/25482478.jpg','https://www.youtube.com/embed/9UOADuWmNKs','wer','1549814290000'),(3,'Oplenac','Mesto koje bi trebalo obavezno posetiti pri dolasku u ove krajeve je selo Oplenac koji se nalazi na 6 km od Aranđelovca. Ovde je kralj Petar podigao mauzolej sa ciljem da se tu sahrane svi članovi porodice Karađorđević. Pored mauzoleja, ovde možete videti i Spomenik ustanicima iz Prvog srpskog ustanka, kao i crkvu Svetog Đorđa.','https://www.discoversrem.com/wp-content/uploads/2018/12/1-728x364.jpg','https://www.youtube.com/embed/DY8yhmUUTfM','wer','1549987481000'),(4,'Garaško jezero','Omiljeno izletište građana Aranđelovca udaljeno 10 km od grada je Garaško jezero. Prostire se na površini od 65 ha, a njegova najveća dubina je preko 20 m. Jezero je bogato ribom, a karakterišu ga strme obale sa jedne i pristupačne obale sa druge strane. U njegovoj blizini nalaze se sportski tereni za odbojku na pesku i mali fudbal.','http://www.novosti.rs/upload/images/2015//08/24/bukulja%20(3).jpg','https://www.youtube.com/embed/8tCdGpBnHC4','wer','1551283553000');
/*!40000 ALTER TABLE `izleti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:32
