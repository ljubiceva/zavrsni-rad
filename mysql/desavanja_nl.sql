-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `nl`
--

DROP TABLE IF EXISTS `nl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `nl` (
  `idnl` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(450) NOT NULL,
  `opis` varchar(450) NOT NULL,
  `slika` varchar(450) NOT NULL,
  `opisid` varchar(4500) NOT NULL,
  `datum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idnl`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nl`
--

LOCK TABLES `nl` WRITE;
/*!40000 ALTER TABLE `nl` DISABLE KEYS */;
INSERT INTO `nl` VALUES (1,'Kavana bar','svirka','https://scontent.fbeg5-1.fna.fbcdn.net/v/t1.0-9/37123214_1913327605397018_6884293677351436288_n.jpg?_nc_cat=110&_nc_ht=scontent.fbeg5-1.fna&oh=be4400fb9e04d90d67bcad5ff9da5c28&oe=5CCAB3B5','f','1550230304000'),(2,'Fan klub','svirka','https://scontent.fbeg5-1.fna.fbcdn.net/v/t1.0-9/37123214_1913327605397018_6884293677351436288_n.jpg?_nc_cat=110&_nc_ht=scontent.fbeg5-1.fna&oh=be4400fb9e04d90d67bcad5ff9da5c28&oe=5CCAB3B5','f','1551461720000'),(3,'Bašta','svirka','https://scontent.fbeg5-1.fna.fbcdn.net/v/t1.0-9/37123214_1913327605397018_6884293677351436288_n.jpg?_nc_cat=110&_nc_ht=scontent.fbeg5-1.fna&oh=be4400fb9e04d90d67bcad5ff9da5c28&oe=5CCAB3B5','f','1549987481000');
/*!40000 ALTER TABLE `nl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:30
