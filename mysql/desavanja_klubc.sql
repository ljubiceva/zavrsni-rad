-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `klubc`
--

DROP TABLE IF EXISTS `klubc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klubc` (
  `idklubc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(450) NOT NULL,
  `opis` varchar(450) NOT NULL,
  `slika` varchar(450) NOT NULL,
  `trejler` varchar(450) NOT NULL,
  `opisid` varchar(450) NOT NULL,
  `datum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idklubc`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klubc`
--

LOCK TABLES `klubc` WRITE;
/*!40000 ALTER TABLE `klubc` DISABLE KEYS */;
INSERT INTO `klubc` VALUES (1,'Malterego','Književno veče posvećeno promociji knjige Malterego pisca Marka Šelića - Marchela','https://www.laguna.rs/_img/korice/3714/malterego_knjiga_druga_higijena_nesecanja_ii_deo-marko_selic_marcelo_v.jpg','https://www.youtube.com/embed/GVJsK-zNJmQ','asdf','1549647423000'),(2,'Pakao','Književno veče posvećeno promociji knjige Pakao pisca Vladimira Tabaševića','http://www.laguna.rs/_img/korice/3247/pa_kao-vladimir_tabasevic_v.jpg','https://www.youtube.com/embed/XLIbkIOORB0','asdf','1549987486000');
/*!40000 ALTER TABLE `klubc` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:31
