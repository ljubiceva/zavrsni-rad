-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desavanja
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `izlozbe`
--

DROP TABLE IF EXISTS `izlozbe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `izlozbe` (
  `idizlozbe` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(450) NOT NULL,
  `opis` varchar(4000) NOT NULL,
  `slika` varchar(450) NOT NULL,
  `opisid` varchar(5000) NOT NULL,
  `datum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idizlozbe`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izlozbe`
--

LOCK TABLES `izlozbe` WRITE;
/*!40000 ALTER TABLE `izlozbe` DISABLE KEYS */;
INSERT INTO `izlozbe` VALUES (1,'Izložba fotografija „Nebojša Glogovac“','Od 21. decembra 2018. (19.00) do 5. marta 2019, u Filmskoj galeriji Dvorana Park biće otvorena izložba „Nebojša Glogovac“ koju je priredila Maja Medić.','https://www.danubeogradu.rs/wp-content/uploads/2018/12/izlozba-nebojsa-glogovac-dvorana-kcb.jpg','Na otvaranju izložbe, koja se sastoji od četrdesetak fotografija iz velikog broja filmova i predstava u kojima je Nebojša Glogovac ostvario svoje najznačajnije uloge, o njegovom opusu razgovaraće reditelj Rajko Grlić i fotografkinja Maja Medić, koja je priredila ovu izložbu.','1549814290000'),(2,'Izložba „Press Photo Srbija 2018“','Press Photo Srbija 2018, godišnja izložba nagrađenih fotografija sa 20. nacionalnog konkursa za najbolju medijsku fotografiju, biće otvorena 25. decembra od 19.00.','https://www.danubeogradu.rs/wp-content/uploads/2018/12/press-photo-ssrbija-2018-glavna-nagrada-nemanja-pancic.jpg','U Domu omladine Beograda ovu značajnu izložbu kao i konkurs organizuju Centar za razvoj fotografije i Udruženje novinara Srbije.','1550424608000'),(3,'Narodni muzej: Monografija „Petar Omčikus“ i poklon „Ruševine Zadra“',' predstavljena je monografija „Petar Omčikus“ i, po želji umetnika, njegovo antologijsko platno „Ruševine Zadra“ iz 1947. godine poklonjeno muzeju.','https://www.danubeogradu.rs/wp-content/uploads/2018/11/petar-omcikus-rusevine-zadra.jpg','U Atrijumu Narodnog muzeja predstavljena je monografija „Petar Omčikus“ i, po želji umetnika, njegovo antologijsko platno „Ruševine Zadra“ iz 1947. godine poklonjeno muzeju.','1549474461000');
/*!40000 ALTER TABLE `izlozbe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 12:02:29
